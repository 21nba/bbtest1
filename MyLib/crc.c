/*!*****************************************************************************
 * @file
 * 
 * 
 * @date:		17 июня 2016 г.
 * @author:		matrosov-dp
 ******************************************************************************/

#include "crc.h"
#include "string.h"

/*============================================================================*/

CRC_HandleTypeDef	hCRC;			//!< Аппаратный модуль вычисления CRC

/*!*****************************************************************************
 * Настройка модуля аппаратного вычисления CRC.
 *
 * @return	Результат выполнения функции (см. 'typedef enum fStatus').
 ******************************************************************************/
fStatus CRC_Init(void)
{
	hCRC.Instance = CRC;
	hCRC.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_DISABLE;
	hCRC.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_DISABLE;
	hCRC.Init.GeneratingPolynomial = MBUS_CRC_POLINOMIAL;
	hCRC.Init.CRCLength = MBUS_CRC_LENGTH;
	hCRC.Init.InitValue = MBUS_CRC_INITVALUE;
	hCRC.Init.InputDataInversionMode = MBUS_CRC_INPUTINVERSION;
	hCRC.Init.OutputDataInversionMode = MBUS_CRC_OUTPUTINVERSION;
	hCRC.InputDataFormat = MBUS_CRC_INPUTDATAFORMAT;
	__CHECK_H(HAL_CRC_Init(&hCRC));

	return FUNC_OK;
}

/*!*****************************************************************************
 * Добавляет CRC в конец обозначенного пакета данных.
 *
 * @param	*data	Указатель на пакет данных.
 * @param	size	Размер пакета данных.
 ******************************************************************************/
void CRC_Add(uint8_t *data, uint16_t size)
{
	crc_t crc;

	crc = (crc_t)HAL_CRC_Calculate(&hCRC, (uint32_t*)data, size);

	memcpy(data + size, &crc, MBUS_FIELD_CRC);
}

/*!*****************************************************************************
 * Проверяет целостность пакета данных путем подсчета CRC-значения и сравнения
 * его с CRC-значением, заложенным в конце пакета.
 *
 * @param	*data	Указатель на пакет данных.
 * @param	size	Размер пакета данных.
 *
 * @return		Результат сравнения:
 * 					1	- CRC совпдадают;
 * 					0	- CRC не совпадают.
 ******************************************************************************/
uint8_t CRC_Check(uint8_t *data, uint16_t size)
{
	crc_t calc, orig;

	calc = (crc_t)HAL_CRC_Calculate(&hCRC, (uint32_t*)data, size);

	memcpy(&orig, data + size, MBUS_FIELD_CRC);

	if(calc == orig)	return 1;
	else				return 0;
}

/*!*****************************************************************************
 * __weak
 ******************************************************************************/
void HAL_CRC_MspInit(CRC_HandleTypeDef* hcrc)
{
	if (hcrc->Instance == CRC)
	{
		__HAL_RCC_CRC_CLK_ENABLE();
	}

}

/*!*****************************************************************************
 * __weak
 ******************************************************************************/
void HAL_CRC_MspDeInit(CRC_HandleTypeDef* hcrc)
{
	if (hcrc->Instance == CRC)
	{
		__HAL_RCC_CRC_CLK_DISABLE();
	}
}

