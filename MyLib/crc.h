/*!*****************************************************************************
 * @file
 * 
 * 
 * @date:		17 июня 2016 г.
 * @author:		matrosov-dp
 ******************************************************************************/

#ifndef INC_CRC_H_
#define INC_CRC_H_

#include "main.h"
#include "modbus.h"

/*============================================================================*/

#if (MBUS_FIELD_CRC == 1)
typedef uint8_t crc_t;
#elif (MBUS_FIELD_CRC == 2)
typedef uint16_t crc_t;
#elif (MBUS_FIELD_CRC == 4)
typedef uint32_t crc_t;
#endif

/*============================================================================*/

extern CRC_HandleTypeDef	hCRC;

/*============================================================================*/

fStatus	CRC_Init		(void);

void	CRC_Add			(uint8_t*, uint16_t);
uint8_t	CRC_Check		(uint8_t*, uint16_t);


#endif /* INC_CRC_H_ */
