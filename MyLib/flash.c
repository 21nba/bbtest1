/*!*****************************************************************************
 * @file
 * Здесь функции для работы с памятью FLASH.
 * Во FLASH-памяти будем сохранять константы, которые должны быть сохранены даже
 * в случае отключения питания.
 * 
 * @date:		1 июня 2016 г.
 * @author:		matrosov-dp
 ******************************************************************************/

#include "flash.h"
#include "crc.h"
#include "backup.h"
#include "string.h"

FLASH_HandleTypeDef	flash;

/*============================================================================*/

static fStatus	FLASH_InitDefault			(void);
static fStatus	FLASH_Read_And_Check		(void);
static fStatus	FLASH_CheckFirmwareCRC		(void);

#ifdef SET_READ_PROTECT
static fStatus	FLASH_SetReadProtection		(void);
#endif
#ifdef SET_WRITE_PROTECT
static fStatus	FLASH_SetWriteProtection	(void);
#endif

/*!*****************************************************************************
 * Начальная настройка блока FLASH.
 * Алгоритм:
 * 	1.	Читается область памяти, отведенная под хранение информации о
 * 		состоянии USER_FLASH (4 байта).
 * 	2.	Анализ:
 * 		2.1 Если USER_FLASH не инициализована, то проводится ее инициализация
 * 			значениями по умолчанию (при этом конфигурируется главная структура
 * 			данных 'FLASH_HandleTypeDef flash'). Также происходит подсчет
 * 			контрольной суммы прошивки и ее запись в область USER_FLASH.
 * 		2.2 Если USER_FLASH инициализована, то происходит чтение информации
 * 			из FLASH-памяти в главную структуру 'FLASH_HandleTypeDef flash'.
 * 			Также происходит проверка контрольной суммы прошивки.
 *	3.	Устанавливается защита прошивки от чтения и записи (если требуется).
 *
 * @return	Результат выполнения функции (см. 'typedef enum EVNT_Code').
 *
 * @note	Предварительно должны быть вызваны функции CRC_Init(), CHRONO_Init()
 *
 * @note	Функции FLASH_SetReadProtection() и FLASH_SetWriteProtection() могут
 * 			привести к рестарту микроконтроллера (см. HAL_FLASH_OB_Launch()).
 ******************************************************************************/
EVNT_Code FLASH_Init(void)
{
	uint32_t	key;
	uint8_t		need_init;

	// 1. Подготавливаем главную структуру. Читаем ключевую область памяти.
	memset(&flash, 0, sizeof(flash));
	memcpy(&key, (void*)fUSER_PAGE(0), 4);

	// 2. Оцениваем ключевую область памяти:
	//	  	-	Если она отличается от ожидаемого значения - планируем
	//	  		инициализацию FLASH значениями по умолчанию.
	//	  	-	Если она равна ожидаемому - читаем данные и проверяем
	//			целостность прошивки. Если процедуры чтения и проверки неудачны,
	//			то планируем инициализация FLASH значениями по умолчанию.
	if(key != fFIRMWARE_VERSION)
	{
		need_init = 1;
	}
	else
	{
		if(FLASH_Read_And_Check() == FUNC_OK)	need_init = 0;
		else									need_init = 1;
	}

	// 3. Инициализация FLASH значениями по умолчанию (если требуется)
	if(need_init)
	{
		__CHECK_F_C(FLASH_InitDefault(), ecINIT_FLASH);
		HAL_RTCEx_BKUPWrite(&hCHRONO, BACKUP_RESTART, 0);
		HAL_RTCEx_BKUPWrite(&hCHRONO, BACKUP_CHRONO, 0);
		HAL_RTCEx_BKUPWrite(&hCHRONO, BACKUP_Vaccum, 0);
	}

#ifdef SET_READ_PROTECT
	// 4. Установка защиты  от чтения
	__CHECK_F_C(FLASH_SetReadProtection(), ecINIT_FLASH_Prot);
#endif

#ifdef SET_WRITE_PROTECT
	// 5. Установка защиты от записи
	__CHECK_F_C(FLASH_SetWriteProtection(), ecINIT_FLASH_Prot);
#endif

	return ecOK;
}

/*!*****************************************************************************
 * Записывает данные во FLASH с проверкой успешности записи. Проверка
 * реализована путем сравнения CRC-значений. В случае, если по каким-либо
 * причинам проверка дала ошибку, попытка записать повторяется. Количество
 * попыток ограничено макросом 'fWRITE_ATTEMPT'.
 *
 * @param	address	Адрес FLASH.
 * @param	*src	Отсюда данные для записи.
 * @param	size	Количество байт для записи.
 *
 * @return	Результат выполнения функции (см. 'typedef enum fStatus').
 *
 * @note	Функция написана для записи данных в пределах одной страницы.
 * 			Поэтому параметр 'size' ограничен размером страницы (2048), за
 * 			вычетом поля, отведенного для записи CRC-значения.
 * 			Можно функцию немного модифицировать, если возникнет потребность
 * 			записи нескольких страниц одной функцией.
 ******************************************************************************/
fStatus FLASH_Write(uint32_t address, void *src, uint16_t size)
{
	FLASH_EraseInitTypeDef		fErase;
	uint32_t					fError;

	uint16_t 					*ptr16;
	uint32_t					addr;
	uint16_t					bytes;

	uint32_t					tail;
	crc_t 						crc;
	uint8_t						attempt = fWRITE_ATTEMPT;

	do
	{
		// 1. Контроль количества попыток записи FLASH
		if(attempt-- == 0)
		{
			return FUNC_ERR;
		}

		// 2. Подготовить переменные
		ptr16 = (uint16_t*)src;
		addr = address;
		bytes = size;
		crc = (crc_t)HAL_CRC_Calculate(&hCRC, (uint32_t*)src, size);

		// 3. Разблокировать FLASH
		HAL_FLASH_Unlock();

		// 4. Очистить записываемую страницу (адекватная запись предполагает
		//	  обязательно предварительное очищение)
		fErase.TypeErase	= FLASH_TYPEERASE_PAGES;
		fErase.PageAddress	= addr;
		fErase.NbPages		= 1;
		HAL_FLASHEx_Erase(&fErase, &fError);

		// 5. Записать данные и CRC
		while(bytes >= 2)
		{
			HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, *ptr16);
			ptr16++;
			addr += 2;
			bytes -= 2;
		}

		if(bytes)
		{
			tail =	0xFF000000						+
					(((uint32_t)crc) << 8)			+
					((uint32_t)(*ptr16 & 0x00FF))	;

			ptr16 = (uint16_t*)&tail;
			HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, *ptr16);
			ptr16++;
			addr +=2;
			HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, *ptr16);
		}
		else
		{
			HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, crc);
		}

		// 6. Заблокировать FLASH
		HAL_FLASH_Lock();

		// 7. Прочитать записанное значение CRC
		memcpy(&crc, (void*)(address + size), sizeof(crc_t));

	}while(crc != (crc_t)HAL_CRC_Calculate(&hCRC, (uint32_t*)address, size));

	return FUNC_OK;
}

/*!*****************************************************************************
 * Читает данные из FLASH с проверкой целостности путем сопоставления CRC-
 * значений (прочитанной и вычисленной).
 *
 * @param	addr	Адрес FLASH.
 * @param	*dst	Сюда данные прочитаются.
 * @param	size	Количество байт для чтения (ОДЗ: 0 ... 2046).
 *
 * @return	Результат выполнения функции (см. 'typedef enum fStatus').
 ******************************************************************************/
fStatus FLASH_Read(uint32_t addr, void *dst, uint16_t size)
{
	crc_t crc;

	// 1. Проверить CRC
	memcpy(&crc, (void*)(addr + size), sizeof(crc_t));

	if(crc != (crc_t)HAL_CRC_Calculate(&hCRC, (uint32_t*)addr, size))
	{
		return FUNC_ERR;
	}

	// 2. Прочитать данные
	memcpy(dst, (void*)addr, size);

	return FUNC_OK;
}

/*!*****************************************************************************
 * Инициализует пользовательскую область FLASH-памяти значениями по умолчанию.
 *
 * @return	Результат выполнения функции (см. 'typedef enum fStatus').
 ******************************************************************************/
static fStatus FLASH_InitDefault(void)
{
	// 1. Подготавливаем данные для записи
	memset(&flash, 0, sizeof(flash));
	flash.gr0.version			= fFIRMWARE_VERSION;
	flash.gr0.crc				= (crc_t)HAL_CRC_Calculate(&hCRC,
			(uint32_t*)fSTART_ADDR, (fPAGES_TOTAL - fPAGES_USER) * fPAGE_SIZE);
	CHRONO_Stamp(&flash.gr0.chr);
	memcpy(&flash.gr1.data.clbr, clbr_default, sizeof(flash.gr1.data.clbr));
	flash.gr1.data.clbr_count	= DATA_COUNT_DEFAULT;
	flash.gr1.data.flags		= DATA_FLAGS_DEFAULT;
	flash.gr1.empty.treshold	= EMPTY_TRESHOLD_DEFAULT;
	flash.gr1.empty.period		= EMPTY_PERIOD_DEFAULT;
	flash.gr2.coil.config		= COIL_CONFIG_DEFAULT;
	flash.gr2.Link.id			= LINK_ID_DEFAULT;
#ifdef USE_MFO
	flash.gr2.out.mfo1.mode		= MFO1_MODE_DEFAULT;
	flash.gr2.out.mfo1.p.active	= PULSE_ACTIVE_DEFAULT;
	flash.gr2.out.mfo1.p.weight	= PULSE_WEIGHT_DEFAULT;
	flash.gr2.out.mfo1.p.width	= PULSE_WIDTH_DEFAULT;
	flash.gr2.out.mfo1.f.weight	= FREQ_WEIGHT_DEFAULT;
	flash.gr2.out.mfo1.l.mode	= LOGIC_MODE_DEFAULT;
	flash.gr2.out.mfo1.l.V		= LOGIC_V_DEFAULT;
	flash.gr2.out.mfo1.l.Gtrmin	= LOGIC_Gtrmin_DEFAULT;
	flash.gr2.out.mfo1.l.Gtrmax	= LOGIC_Gtrmax_DEFAULT;
	flash.gr2.out.mfo1.l.active	= LOGIC_ACTIVE_DEFAULT;
	flash.gr2.out.mfo2.mode		= MFO2_MODE_DEFAULT;
	flash.gr2.out.mfo2.p.active	= PULSE_ACTIVE_DEFAULT;
	flash.gr2.out.mfo2.p.weight	= PULSE_WEIGHT_DEFAULT;
	flash.gr2.out.mfo2.p.width	= PULSE_WIDTH_DEFAULT;
	flash.gr2.out.mfo2.f.weight	= FREQ_WEIGHT_DEFAULT;
	flash.gr2.out.mfo2.l.mode	= LOGIC_MODE_DEFAULT;
	flash.gr2.out.mfo2.l.V		= LOGIC_V_DEFAULT;
	flash.gr2.out.mfo2.l.Gtrmin	= LOGIC_Gtrmin_DEFAULT;
	flash.gr2.out.mfo2.l.Gtrmax	= LOGIC_Gtrmax_DEFAULT;
	flash.gr2.out.mfo2.l.active	= LOGIC_ACTIVE_DEFAULT;
	flash.gr2.out.cur.Imin		= CURR_Imin_DEFAULT;
	flash.gr2.out.cur.Imax		= CURR_Imax_DEFAULT;
	flash.gr2.out.cur.Ki		= CURR_Ki_DEFAULT;
	flash.gr2.out.flags			= OUT_FLAGS_DEFAULT;
#endif
	flash.gr2.conf.Gmin			= CONF_Gmin_DEFAULT;
	flash.gr2.conf.Gmax			= CONF_Gmax_DEFAULT;
	flash.gr2.conf.Nmin			= CONF_Nmin_DEFAULT;
	flash.gr2.conf.Nmax			= CONF_Nmax_DEFAULT;
	flash.gr2.conf.Nmaxt		= CONF_Nmaxt_DEFAULT;

	// 2. Записываем.
	//	  Обрати внимание: группа констант 'CONST_GROUP0' записывается в
	//	  последнюю очередь.
	__CHECK_F_FLASHWRITE_gr(1);
	__CHECK_F_FLASHWRITE_gr(2);
	__CHECK_F_FLASHWRITE_gr(3);
	__CHECK_F_FLASHWRITE_gr(0);

	return FUNC_OK;
}

/*!*****************************************************************************
 * Читает пользовательские данные из FLASH, а также проверяет целостность
 * прошивки.
 ******************************************************************************/
static fStatus FLASH_Read_And_Check(void)
{
	__CHECK_F_FLASHREAD_gr(0);
	__CHECK_F_FLASHREAD_gr(1);
	__CHECK_F_FLASHREAD_gr(2);
	__CHECK_F_FLASHREAD_gr(3);

	__CHECK_F(FLASH_CheckFirmwareCRC());

	return FUNC_OK;
}

/*!*****************************************************************************
 * Проверяет контрольную сумму прошивки. Алгоритм:
 * 		-	Вычисляется контрольная сумма всей FLASH-памяти, за исключением
 * 			пользовательской области.
 * 		-	Полученное значение сверяется со значением, предварительно
 * 			записанным в пользовательской области FLASH-памяти.
 *
 * @return	Результат выполнения функции (см. 'typedef enum fStatus').
 ******************************************************************************/
static fStatus FLASH_CheckFirmwareCRC(void)
{
	crc_t crc = (crc_t)HAL_CRC_Calculate(&hCRC, (uint32_t*)fSTART_ADDR,
						(fPAGES_TOTAL - fPAGES_USER) * fPAGE_SIZE);

	if(crc != flash.gr0.crc)	return FUNC_ERR;
	else						return FUNC_OK;
}

#ifdef SET_READ_PROTECT
/*!*****************************************************************************
 * Устанавливает защиту прошивки от чтения.
 *
 * @return	Результат выполнения функции (см. 'typedef enum fStatus').
 ******************************************************************************/
static fStatus FLASH_SetReadProtection(void)
{
	FLASH_OBProgramInitTypeDef	ob;

	HAL_FLASHEx_OBGetConfig(&ob);

	if(ob.RDPLevel != OB_RDP_LEVEL_1)
	{
		__CHECK_H(HAL_FLASH_Unlock());
		__CHECK_H(HAL_FLASH_OB_Unlock());

		ob.OptionType	= OPTIONBYTE_RDP;
		ob.RDPLevel		= OB_RDP_LEVEL_1;
		__CHECK_H(HAL_FLASHEx_OBProgram(&ob));

		__CHECK_H(HAL_FLASH_OB_Launch());

		__CHECK_H(HAL_FLASH_OB_Lock());
		__CHECK_H(HAL_FLASH_Lock());
	}

	return FUNC_OK;
}
#endif

#ifdef SET_WRITE_PROTECT
/*!*****************************************************************************
 * Устанавливает защиту прошивки от записи.
 *
 * @return	Результат выполнения функции (см. 'typedef enum fStatus').
 ******************************************************************************/
static fStatus FLASH_SetWriteProtection(void)
{
	FLASH_OBProgramInitTypeDef	ob;

	HAL_FLASHEx_OBGetConfig(&ob);

	if(ob.WRPPage != (~fWRP_PAGES))
	{
		__CHECK_H(HAL_FLASH_Unlock());
		__CHECK_H(HAL_FLASH_OB_Unlock());

		ob.OptionType	= OPTIONBYTE_WRP;
		ob.WRPState		= OB_WRPSTATE_ENABLE;
		ob.WRPPage		= fWRP_PAGES;
		__CHECK_H(HAL_FLASHEx_OBProgram(&ob));

		__CHECK_H(HAL_FLASH_OB_Launch());

		__CHECK_H(HAL_FLASH_OB_Lock());
		__CHECK_H(HAL_FLASH_Lock());
	}

	return FUNC_OK;
}
#endif

