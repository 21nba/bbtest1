/*!*****************************************************************************
 * @file
 * 
 *
 * @date:		1 июня 2016 г.
 * @author:		matrosov-dp
 ******************************************************************************/

#ifndef INC_FLASH_H_
#define INC_FLASH_H_

#include "main.h"
#include "event.h"
#include "crc.h"
#include "data.h"
#include "empty.h"
#include "coil.h"
#include "link.h"
#include "config.h"
#include "chrono.h"
#ifdef USE_MFO
#include "out.h"
#endif

/*============================================================================*/
/* begin	Макросы, которые необходимо обязательно проверить!
/*============================================================================*/
#define fPAGES_USER				20			// Кол-во пользователських страниц
#define fWRITE_ATTEMPT			3			// Кол-во попыток записи FLASH
/*============================================================================*/
/* end		Макросы, которые необходимо обязательно проверить!
/*============================================================================*/

#define fWRP_PAGES				(	OB_WRP_PAGES0TO15MASK 	|				\
									OB_WRP_PAGES16TO31MASK	|				\
									OB_WRP_PAGES32TO47MASK	|				\
									OB_WRP_PAGES48TO49		|				\
									OB_WRP_PAGES50TO51		|				\
									OB_WRP_PAGES52TO53		|				\
									OB_WRP_PAGES54TO55		|				\
									OB_WRP_PAGES56TO57		|				\
									OB_WRP_PAGES58TO59		|				\
									OB_WRP_PAGES60TO61			)

#define fPAGE_ADDR(N)			(fSTART_ADDR + ((N) * fPAGE_SIZE))
#define fUSER_PAGE(N)			fPAGE_ADDR(fPAGES_TOTAL - fPAGES_USER + (N))

#define grSIZE(N)				sizeof(CONST_Group##N##TypeDef)

#define __CHECK_F_FLASHWRITE_gr(N)											\
{																			\
	if(FLASH_Write(fUSER_PAGE(upCONST##N),&flash.gr##N,grSIZE(N)) != FUNC_OK)\
		return FUNC_ERR;													\
}
#define __CHECK_F_FLASHREAD_gr(N)											\
{																			\
	if(FLASH_Read(fUSER_PAGE(upCONST##N),&flash.gr##N,grSIZE(N)) != FUNC_OK)\
		return FUNC_ERR;													\
}
#define __CHECK_RETURN_FLASHWRITE_gr(N)										\
{																			\
	if(FLASH_Write(fUSER_PAGE(upCONST##N),&flash.gr##N,grSIZE(N)) != FUNC_OK)\
	{																		\
		EVNT_Fix(ecINTFLASH, esERROR);										\
		return;																\
	}																		\
	else																	\
	{																		\
		EVNT_Fix(ecINTFLASH, esNOERROR);									\
	}																		\
}
#define __CHECK_RETURN_FLASHREAD_gr(N)										\
{																			\
	if(FLASH_Read(fUSER_PAGE(upCONST##N),&flash.gr##N,grSIZE(N)) != FUNC_OK)\
	{																		\
		EVNT_Fix(ecINTFLASH, esERROR);										\
		return;																\
	}																		\
	else																	\
	{																		\
		EVNT_Fix(ecINTFLASH, esNOERROR);									\
	}																		\
}

/*============================================================================*/

enum FLASH_UserPage
{
	upCONST0,
	upCONST1,
	upCONST2,
	upCONST3,
	upEVNT0,
	upEVNT1,
	upEVNT2,
	upEVNT3,
	upEVNT4
};

/*!*****************************************************************************
 * Главная группа констант. Размер не должен превышать 'FLASH_PAGE_SIZE'.
 ******************************************************************************/
#pragma pack(push, 1)
typedef struct
{
	uint32_t			version;	/*!< Версия прошивки					*/
	crc_t				crc;		/*!< Контрольная сумма прошивки			*/
	CHRONO_StampTypeDef	chr;		/*!< Метка времени						*/
} CONST_Group0TypeDef;

/*!*****************************************************************************
 * Первая группа констант. Размер не должен превышать 'FLASH_PAGE_SIZE'.
 ******************************************************************************/
typedef struct
{
	DATA_ConstTypeDef	data;		/*!< Константы блока 'Data'				*/
	EMPTY_ConstTypeDef	empty;		/*!< Константы блока 'Empty'			*/
	uint32_t			serial;		/*!< Серийный номер устройства			*/
} CONST_Group1TypeDef;

/*!*****************************************************************************
 * Вторая группа констант. Размер не должен превышать 'FLASH_PAGE_SIZE'.
 ******************************************************************************/
typedef struct
{
	COIL_ConstTypeDef	coil;		/*!< Константы блока 'Coil'				*/
	LINK_ConstTypeDef	Link;		/*!< Константы блока 'NET'				*/
#ifdef USE_MFO
	OUT_ConstTypeDef	out;		/*!< Константы блока 'Out'				*/
#endif
	CONF_ConstTypeDef	conf;		/*!< Константы блока 'Config'			*/
} CONST_Group2TypeDef;

/*!*****************************************************************************
 * Третья группа констант. Размер не должен превышать 'FLASH_PAGE_SIZE'.
 ******************************************************************************/
typedef struct
{
	DATA_ArchiveTypeDef	data;		/*!< Архивирование данных блока 'Data'	*/
} CONST_Group3TypeDef;
#pragma pack(pop)

/*!*****************************************************************************
 * Главная структура: пользовательская область встроенной FLASH-памяти.
 ******************************************************************************/
typedef struct
{
	CONST_Group0TypeDef gr0;		/*!< Главная группа констант			*/
	CONST_Group1TypeDef gr1;		/*!< Первая группа констант				*/
	CONST_Group2TypeDef gr2;		/*!< Вторая группа констант				*/
	CONST_Group3TypeDef gr3;		/*!< Третья группа констант				*/
} FLASH_HandleTypeDef;

/*============================================================================*/

extern FLASH_EraseInitTypeDef	fErase;
extern FLASH_HandleTypeDef		flash;

/*============================================================================*/

EVNT_Code	FLASH_Init		(void);

fStatus		FLASH_Read		(uint32_t, void*, uint16_t);
fStatus		FLASH_Write		(uint32_t, void*, uint16_t);


#endif /* INC_FLASH_H_ */
