/*!*****************************************************************************
 * @file
 * Test UART.
 *
 * @note	См. 'readme.txt'
 ******************************************************************************/

#include <ax8052.h>
#include <libmftypes.h>
#include <libmfflash.h>
#include <libmfwtimer.h>
#include <libminikitleds.h>

#include <libmfuart.h>
#include <libmfuart0.h>

#define TEST_UART_RX
#define TEST_UART_TX

void main(void)
{
	crc_init();

	crc(1);
}