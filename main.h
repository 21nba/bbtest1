/*!*****************************************************************************
 * @file
 * 
 * 
 * @date:		19 апр. 2016 г.
 * @author:		matrosov-dp
 ******************************************************************************/

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

/*============================================================================*/

typedef enum
{
	FUNC_OK,
	FUNC_ERR,
	FUNC_ARG
}fStatus;

/*============================================================================*/


#define __MIN(n,m) 				(((n) < (m)) ? (n) : (m))
#define __MAX(n,m) 				(((n) > (m)) ? (n) : (m))

/*============================================================================*/

#ifdef STM32F373xC

#include "stm32f3xx_hal.h"

#define CPU_FREQUENCE			48000000

/*-----	Debugger -------------------------------------------------------------*/

#define DBGGPIO_CLKEN()			__HAL_RCC_GPIOA_CLK_ENABLE();				\
								__HAL_RCC_GPIOB_CLK_ENABLE()


/*-----	LSE ------------------------------------------------------------------*/

#define LSEGPIO_CLKEN()			__HAL_RCC_GPIOC_CLK_ENABLE()


/*-----	HSE ------------------------------------------------------------------*/

#define HSEGPIO_CLKEN()			__HAL_RCC_GPIOF_CLK_ENABLE()


/*-----	Таймер, используемый функцией HAL_GetTick() --------------------------*/

#define HGTTIM					TIM13
#define HGTTIM_CLKEN()			__HAL_RCC_TIM13_CLK_ENABLE()
#define HGTTIM_CLKDIS()			__HAL_RCC_TIM13_CLK_DISABLE()
#define HGTTIM_DBG()														\
{																			\
	SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM13_STOP);					\
}


/*-----	Светодиоды -----------------------------------------------------------*/

#define LEDTIM					TIM4
#define LEDTIM_CHN				TIM_CHANNEL_4
#define LEDTIM_CCRx				CCR4
#define LEDTIM_CLKEN()			__HAL_RCC_TIM4_CLK_ENABLE()
#define LEDTIM_CLKDIS()			__HAL_RCC_TIM4_CLK_DISABLE()
#define LEDTIM_DBG()														\
{																			\
	SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM4_STOP);					\
}

#define LEDPORT					GPIOF
#define LEDPIN					GPIO_PIN_6
#define LEDAF					GPIO_AF2_TIM4
#define LEDGPIO_CLKEN()			__HAL_RCC_GPIOF_CLK_ENABLE()


/*-----	Кнопки ---------------------------------------------------------------*/

#define BUTTIM					TIM18
#define BUTTIM_IRQn				TIM18_DAC2_IRQn
#define BUTTIM_IRQHandler		TIM18_DAC2_IRQHandler
#define BUTTIM_CLKEN()			__HAL_RCC_TIM18_CLK_ENABLE()
#define BUTTIM_CLKDIS()			__HAL_RCC_TIM18_CLK_DISABLE()
#define BUTTIM_DBG()														\
{																			\
	SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM18_STOP);					\
}

#define BUTPORT					GPIOF
#define BUTPIN_SELECT			GPIO_PIN_6
#define BUTPIN_JUMP				GPIO_PIN_7
#define BUTEXT_IRQn				EXTI9_5_IRQn
#define BUTEXT_IRQHandler		EXTI9_5_IRQHandler
#define BUTGPIO_CLKEN()			__HAL_RCC_GPIOF_CLK_ENABLE()


/*-----	Дисплей --------------------------------------------------------------*/

#define LCDPORT_DATA			GPIOC
#define LCDPORT_CONTROL			GPIOA
#define LCDPIN_RS				GPIO_PIN_0
#define LCDPIN_E				GPIO_PIN_1
#define LCDGPIO_CLKEN()			__HAL_RCC_GPIOA_CLK_ENABLE();				\
								__HAL_RCC_GPIOC_CLK_ENABLE()


/*-----	Внешний АЦП ----------------------------------------------------------*/

#define EXTADCSPI				SPI3
#define EXTADCSPI_CLKEN()		__HAL_RCC_SPI3_CLK_ENABLE()
#define EXTADCSPI_CLKDIS()		__HAL_RCC_SPI3_CLK_DISABLE()

#define EXTADCPORTSPI			GPIOA
#define EXTADCPIN_SCK			GPIO_PIN_1
#define EXTADCPIN_MISO			GPIO_PIN_2
#define EXTADCPIN_MOSI			GPIO_PIN_3
#define EXTADCAF				GPIO_AF6_SPI3

#define EXTADCPORT				GPIOB
#define EXTADCPIN_READY			GPIO_PIN_7
#define EXTADCPIN_CS			GPIO_PIN_6
#define EXTADCPIN_RESET			GPIO_PIN_8
#define EXTADCGPIO_CLKEN()		__HAL_RCC_GPIOB_CLK_ENABLE()


/*-----	Управление драйвером катушки и измерение тока катушки ----------------*/

#define COILTIM					TIM3
#define COILTIM_IRQn			TIM3_IRQn
#define COILTIM_IRQHandler		TIM3_IRQHandler
#define COILTIM_CLKEN()			__HAL_RCC_TIM3_CLK_ENABLE()
#define COILTIM_CLKDIS()		__HAL_RCC_TIM3_CLK_DISABLE()
#define COILTIM_DBG()														\
{																			\
	SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM3_STOP);					\
}

#define COILSDADC				SDADC1
#define COILSDADC_CHANNEL		SDADC_CHANNEL_6
#define COILSDADC_PERIPH		RCC_PERIPHCLK_SDADC
#define COILSDADC_ANALOG		PWR_SDADC_ANALOG1
#define COILSDADC_CLKEN()		__HAL_RCC_SDADC1_CLK_ENABLE()
#define COILSDADC_CLKDIS()		__HAL_RCC_SDADC1_CLK_DISABLE()

#define COILSDADCPORT			GPIOB
#define COILSDADCPIN_P			GPIO_PIN_0
#define COILSDADCPIN_M			GPIO_PIN_1

#define COILPORT_EN				GPIOD
#define COILPIN_EN				GPIO_PIN_8
#define COILPORT_FORW			GPIOB
#define COILPIN_FORW			GPIO_PIN_14
#define COILPORT_REV			GPIOB
#define COILPIN_REV				GPIO_PIN_15
#define COILGPIO_CLKEN()		__HAL_RCC_GPIOB_CLK_ENABLE();				\
								__HAL_RCC_GPIOD_CLK_ENABLE()


/*-----	Детектор наличия жидкости в трубе ------------------------------------*/

#define EMPTYPORT				GPIOE
#define EMPTYPIN_TRY1			GPIO_PIN_8
#define EMPTYPIN_TRY2			GPIO_PIN_9
#define EMPTY_CLKEN()			__HAL_RCC_GPIOE_CLK_ENABLE()


/*-----	Связь с внешними устройствами (интерфейс RS485) ----------------------*/

#define RS485TIM				TIM12
#define RS485TIM_IRQn			TIM12_IRQn
#define RS485TIM_IRQHandler		TIM12_IRQHandler
#define RS485TIM_CLKEN()		__HAL_RCC_TIM12_CLK_ENABLE()
#define RS485TIM_CLKDIS()		__HAL_RCC_TIM12_CLK_DISABLE()
#define RS485TIM_DBG()														\
{																			\
	SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM12_STOP);					\
}

#define RS485UART				USART1
#define RS485UARTIRQn			USART1_IRQn
#define RS485UART_IRQHandler	USART1_IRQHandler
#define RS485UART_PERIPHCLK		RCC_PERIPHCLK_USART1
#define RS485UART_CLKSELECTION	Usart1ClockSelection
#define RS485UART_CLKSOURCE		RCC_USART1CLKSOURCE_PCLK2
#define RS485UART_CLKEN()		__HAL_RCC_USART1_CLK_ENABLE()
#define RS485UART_CLKDIS()		__HAL_RCC_USART1_CLK_DISABLE()

#define RS485DMATX				DMA1_Channel4
#define RS485DMARX				DMA1_Channel5
#define RS485DMATX_IRQn			DMA1_Channel4_IRQn
#define RS485DMARX_IRQn			DMA1_Channel5_IRQn
#define RS485DMATX_IRQHandler	DMA1_Channel4_IRQHandler
#define RS485DMARX_IRQHandler	DMA1_Channel5_IRQHandler
#define RS485DMA_CLKEN()		__HAL_RCC_DMA1_CLK_ENABLE()

#define RS485PORTUART			GPIOA
#define RS485PIN_TX				GPIO_PIN_9
#define RS485PIN_RX				GPIO_PIN_10
#define RS485AF					GPIO_AF7_USART1

#define RS485PORT_DIR			GPIOA
#define RS485PIN_DIR			GPIO_PIN_8
#define RS485GPIO_CLKEN()		__HAL_RCC_GPIOA_CLK_ENABLE()


/*-----	Связь с внешними устройствами (интерфейс USB) ------------------------*/

#define USBPORT_CONN			GPIOF
#define USBPIN_CONN				GPIO_PIN_7
#define USBGPIO_CLKEN()			__HAL_RCC_GPIOF_CLK_ENABLE()


/*-----	Многофункциональный выход №1 -----------------------------------------*/

#define MFO1TIM					TIM2
#define MFO1TIM_CHN				TIM_CHANNEL_1
#define MFO1TIM_CCRx			CCR1
#define MFO1TIM_IRQn			TIM2_IRQn
#define MFO1TIM_IRQHandler		TIM2_IRQHandler
#define MFO1TIM_CLKEN()			__HAL_RCC_TIM2_CLK_ENABLE()
#define MFO1TIM_CLKDIS()		__HAL_RCC_TIM2_CLK_DISABLE()
#define MFO1TIM_DBG()														\
		{																	\
			SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM2_STOP);			\
		}

#define MFO1PORT				GPIOA
#define MFO1PIN					GPIO_PIN_0
#define MFO1AF					GPIO_AF1_TIM2
#define MFO1GPIO_CLKEN()		__HAL_RCC_GPIOA_CLK_ENABLE()


/*-----	Многофункциональный выход №2 -----------------------------------------*/

#define MFO2TIM					TIM5
#define MFO2TIM_CHN				TIM_CHANNEL_4
#define MFO2TIM_CCRx			CCR4
#define MFO2TIM_IRQn			TIM5_IRQn
#define MFO2TIM_IRQHandler		TIM5_IRQHandler
#define MFO2TIM_CLKEN()			__HAL_RCC_TIM5_CLK_ENABLE()
#define MFO2TIM_CLKDIS()		__HAL_RCC_TIM5_CLK_DISABLE()
#define MFO2TIM_DBG()														\
		{																	\
			SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM5_STOP);			\
		}

#define MFO2PORT				GPIOA
#define MFO2PIN					GPIO_PIN_13
#define MFO2AF					GPIO_AF2_TIM5
#define MFO2GPIO_CLKEN()		__HAL_RCC_GPIOA_CLK_ENABLE()


/*-----	Вспомогательный таймер блока 'out' (для многофункциональных выходов) -*/

#define OUTTIM					TIM6
#define OUTTIM_IRQn				TIM6_DAC1_IRQn
#define OUTTIM_IRQHandler		TIM6_DAC1_IRQHandler
#define OUTTIM_CLKEN()			__HAL_RCC_TIM6_CLK_ENABLE()
#define OUTTIM_CLKDIS()			__HAL_RCC_TIM6_CLK_DISABLE()
#define OUTTIM_DBG()														\
		{																	\
			SET_BIT(DBGMCU->APB1FZ, DBGMCU_APB1_FZ_DBG_TIM6_STOP);			\
		}


/*-----	Служебный пин (использвоан для индикации загрузки процессора) --------*/

#define CPUCNTRPORT				GPIOB
#define CPUCNTRPIN				GPIO_PIN_2
#define CPUCNTR_CLKEN()			__HAL_RCC_GPIOA_CLK_ENABLE()

#endif	/* STM32F373xC	*/

#endif /* INC_MAIN_H_ */
